module.exports = {
  preset: "ts-jest",
  transform: {
    "^.+\\.(js|ts|tsx)$": "babel-jest",
    "\\.(css|less|scss|sass)$": "jest-transform-css",
  },
  testEnvironment: "jsdom",
  testPathIgnorePatterns: ["<rootDir>/.next/", "<rootDir>/node_modules/"],
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.jest.json",
    },
  },
  setupFilesAfterEnv: ["<rootDir>/jest-setup.ts"],
};
