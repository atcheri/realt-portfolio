import { rest } from "msw";

import tokenList from "./tokenList_response.json";
import tokens from "./tokens_response.json";

export const handlers = [
  rest.get("/tokenList", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tokenList));
  }),
  rest.get("/tokens", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tokens));
  }),
];
