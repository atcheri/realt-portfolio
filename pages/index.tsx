import Link from "next/link";
import { withTranslation } from "../i18n";

import styles from "../styles/Home.module.css";
const Home = ({ t }) => {
  return (
    <main className={styles.main}>
      <div className={styles.title}>
        <Link href="/portfolio">
          <a>{t("Go to")} portfolio</a>
        </Link>
      </div>
    </main>
  );
};

Home.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation("common")(Home);
