import App from "next/app";
import { Web3ReactProvider } from "@web3-react/core";

import AppHead from "../common/components/AppHead";
import Layout from "../common/components/Layout";
import { appWithTranslation } from "../i18n";
import getLibrary from "../common/utils/getLibrary";

import "../styles/globals.css";

if (
  process.env.NODE_ENV === "development" &&
  process.env.NEXT_PUBLIC_API_MOCKING === "enabled"
) {
  require("../mocks");
}

const MyApp = ({ Component, pageProps }) => (
  <>
    <AppHead />
    <Web3ReactProvider getLibrary={getLibrary}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Web3ReactProvider>
  </>
);

MyApp.getInitialProps = async (appContext) => ({
  ...(await App.getInitialProps(appContext)),
});

export default appWithTranslation(MyApp);
