import { useReducer } from "react";

import { withTranslation } from "../i18n";
import reducer from "../modules/Portfolio/portfolio.reducer";
import initialState from "../modules/Portfolio/portfolio.state";
import SeriesList from "../modules/Portfolio/SerieList";
import AccountConnector from "../common/components/AccountConnector";

export const Portfolio = ({ t }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <>
      <AccountConnector t={t} />
      <SeriesList t={t} />
    </>
  );
};

Portfolio.getInitialProps = async () => ({
  namespacesRequired: ["address-form"],
});

export default withTranslation("address-form")(Portfolio);
