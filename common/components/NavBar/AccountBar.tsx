import { useWeb3React } from "@web3-react/core";

const AccountBar = ({ t }) => {
  const { chainId, account, deactivate, active, error } = useWeb3React();

  const isActiveOrError = !active || error;
  if (isActiveOrError) {
    return <div>{t("Not Connected")}</div>;
  }

  return (
    <>
      <div>Chain ID: {chainId}</div>
      <div>
        {t("Address")} : {account}
      </div>
      <div>
        {t("Balance")} : {account}
      </div>
      <div>{`${t("Status")}: ${t("Connected")}`}</div>
      <div>
        Action: <button onClick={deactivate}>{t("Disconnect")}</button>
      </div>
    </>
  );
};

export default AccountBar;
