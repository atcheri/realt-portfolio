import Image from "next/image";
import Link from "next/link";
import styled from "styled-components";

import AccountBar from "./AccountBar";
import { withTranslation } from "../../../i18n";

const NavInner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const NavLeft = styled.div``;

const NavRight = styled.div``;

const NavBar = ({ t }) => {
  return (
    <nav>
      <NavInner>
        <NavLeft>
          <Image
            className="logo"
            src="https://cdn1.realt.co/wp-content/uploads/2019/04/RealT_Logo.svg"
            alt="Realt"
            width={160}
            height={80}
          />
          <Link href="https://realt.co/marketplace/">
            <a>{t("Marketplace")}</a>
          </Link>
          <Link href="https://realt.co/portfolio/?app=sell-tokens">
            <a>{t("Sell-tokens")}</a>
          </Link>
          <Link href="https://realt.co/faq/">
            <a>{t("Faq")}</a>
          </Link>
          <Link href="https://realt.co/education/">
            <a>{t("Education")}</a>
          </Link>
          <Link href="https://realt.co/team/">
            <a>{t("Team")}</a>
          </Link>
          <Link href="https://realt.co/blog/">
            <a>{t("Blog")}</a>
          </Link>
        </NavLeft>
        <NavRight>
          <AccountBar t={t} />
        </NavRight>
      </NavInner>
    </nav>
  );
};

NavBar.getInitialProps = async () => ({
  namespacesRequired: ["common", "navbar"],
});

export default withTranslation("common")(NavBar);
