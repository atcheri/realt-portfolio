import Image from "next/image";
import { withTranslation } from "next-i18next";

import { i18n } from "../../i18n";

const Footer = ({ t }) => {
  return (
    <footer>
      <a
        href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
        target="_blank"
        rel="noopener noreferrer"
      >
        Powered by{" "}
        <Image
          src="https://cdn1.realt.co/wp-content/uploads/2019/04/RealT_Logo.svg"
          alt="Realt"
          width={80}
          height={40}
        />
      </a>
      <div>
        <button
          type="button"
          onClick={() =>
            i18n.changeLanguage(i18n.language === "en" ? "fr" : "en")
          }
        >
          {t("Change-locale")} ({i18n.language})
        </button>
      </div>
    </footer>
  );
};

export default withTranslation("navbar")(Footer);
