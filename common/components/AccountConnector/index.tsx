import { useState, useEffect } from "react";
import { useWeb3React } from "@web3-react/core";
import { injected, walletconnect } from "./connectors";

enum ConnectorNames {
  Injected = "Injected",
  // Network = 'Network',
  WalletConnect = "WalletConnect",
}

const connectorsByName: { [connectorName in ConnectorNames]: any } = {
  [ConnectorNames.Injected]: injected,

  [ConnectorNames.WalletConnect]: walletconnect,
};

const DeactivateButton = ({ t, deactivate, setActivatingConnector }) => {
  return (
    <button
      onClick={() => {
        deactivate();
        setActivatingConnector(undefined);
      }}
    >
      {t("Disconnect")}
    </button>
  );
};

const AccountConnector = ({ t }) => {
  const { active, connector, activate, error, deactivate } = useWeb3React();
  const [activatingConnector, setActivatingConnector] = useState<any>();
  useEffect(() => {
    if (activatingConnector && activatingConnector === connector) {
      setActivatingConnector(undefined);
    }
  }, [activatingConnector, connector]);

  if (active) {
    return (
      <DeactivateButton
        t={t}
        deactivate={deactivate}
        setActivatingConnector={setActivatingConnector}
      />
    );
  }

  if (error) {
    console.error(error);
  }

  return (
    <>
      <h3>{t("Connect to")}:</h3>
      {Object.keys(connectorsByName).map((name) => {
        const currentConnector = connectorsByName[name];
        const connected = currentConnector === connector;
        const activating = currentConnector === activatingConnector;
        const disabled =
          /*!triedEager ||*/ !!activatingConnector || connected || !!error;
        return (
          <button
            disabled={disabled}
            key={name}
            onClick={() => {
              setActivatingConnector(currentConnector);
              activate(connectorsByName[name]);
            }}
          >
            <div>{activating && <span>Activating</span>}</div>
            {name}
          </button>
        );
      })}
      {error && (
        <DeactivateButton
          t={t}
          deactivate={deactivate}
          setActivatingConnector={setActivatingConnector}
        />
      )}
    </>
  );
};

export default AccountConnector;
