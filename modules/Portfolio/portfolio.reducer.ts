import initialState, { PortfolioState } from "./portfolio.state";

type PortfolioResetAction = { type: "reset" };
type PortfolioChangeAction = { type: "change"; address: string };

type PortfolioAction = PortfolioResetAction | PortfolioChangeAction;

const reducer = (state: PortfolioState, action: PortfolioAction) => {
  switch (action.type) {
    case "change":
      return { address: action.address };
    case "reset":
      return initialState;
    default:
      return state;
  }
};

export default reducer;
