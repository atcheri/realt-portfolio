import useFetch from "../../../common/hooks/use-fetch";

const apiUrl = process.env.NEXT_PUBLIC_REALT_API_URL || "http://localhost";

interface ISerieDate {
  date: string;
  timezone_type: number;
  timezone: string;
}
interface ISerie {
  currency: string;
  ethereumContract: string;
  fullName: string;
  lastUpdate: ISerieDate;
  shortName: string;
  symbol: string;
  tokenPrice: number;
}

const SeriesList = ({ t }) => {
  const { loading, error, data = [] } = useFetch<ISerie[]>({
    url: `${apiUrl}/tokens`,
  });

  if (error) {
    return <div aria-label="error-serie-list">Error while loading.</div>;
  }

  if (loading) {
    return <div aria-label="loading-serie-list">Loading ... </div>;
  }

  return (
    <>
      <h1>Series list</h1>
      {data && (
        <ul aria-label="serie-list">
          {data.map((serie) => {
            return (
              <li key={`serie-${serie.shortName}`}>Name: {serie.fullName}</li>
            );
          })}
        </ul>
      )}
    </>
  );
};

export default SeriesList;
