import { render, screen, waitFor } from "@testing-library/react";

import SeriesList from "..";
import { rest, server } from "../../../../mocks/server";

const doNothing = () => null;

beforeAll(() => server.listen());
afterAll(() => server.close());

describe("SerieList", () => {
  afterEach(() => {
    server.resetHandlers();
  });
  describe("[ERROR]", () => {
    it("Shows a div with the error message", async () => {
      const testErrorMessage = "THIS IS A TEST FAILURE";
      server.use(
        rest.get("/tokens", async (req, res, ctx) => {
          return res(ctx.status(500), ctx.json({ error: testErrorMessage }));
        })
      );
      render(<SeriesList t={doNothing} />);
      await waitFor(() => {
        screen.getByLabelText("error-serie-list");
      });
    });
  });
  describe("[NO ERROR]", () => {
    it("Shows a list of serie", async () => {
      render(<SeriesList t={doNothing} />);
      await waitFor(() => {
        expect(
          screen.queryByLabelText("error-serie-list")
        ).not.toBeInTheDocument();
        expect(screen.queryByLabelText("serie-list")).toBeInTheDocument();
      });
    });
  });
});
