import { ChangeEvent, FormEvent, useState } from "react";

import style from "./AddressForm.module.scss";

export const USER_ADDRESS_ID = "user-address";

const AddressForm = ({ t, dispatch }) => {
  const [address, setAddress] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [hasError, setHasError] = useState(false);

  const validate = (value: string): boolean => {
    if (!value.startsWith("0x")) {
      return false;
    }
    return true;
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e;
    setAddress(value);
    if (!value || value.length < 3) {
      return false;
    }
    const valid = validate(value);
    setHasError(!valid);
    if (valid) {
      setDisabled(false);
      return;
    }
    setDisabled(true);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    dispatch({ type: "change", address });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor={USER_ADDRESS_ID}>{t("Enter Address")}</label>
      <input
        aria-label={USER_ADDRESS_ID}
        id={USER_ADDRESS_ID}
        type="text"
        placeholder="0x"
        onChange={handleChange}
        value={address}
      />
      {hasError && (
        <div className={style.error}>{t("Wrong address format error")}</div>
      )}
      <br />
      <button type="submit" aria-label="submit" disabled={disabled}>
        {t("Search")}
      </button>
    </form>
  );
};

export default AddressForm;
