import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import AddressForm, { USER_ADDRESS_ID } from "../";

const doNothing = () => null;

const renderComponent = (spy = doNothing) => {
  render(<AddressForm t={(key: string) => key} dispatch={spy} />);
};

describe("AddressForm", () => {
  describe("[ERROR]", () => {
    describe("Given an empty or short address in the input", () => {
      it("Doesn't show the error message", async () => {
        renderComponent();
        const input = screen.getByRole("textbox", { name: USER_ADDRESS_ID });
        userEvent.type(input, "d");
        expect(input).toHaveValue("d");
        expect(screen.getByRole("button", { name: /submit/i })).toBeDisabled();
        await waitFor(() => {
          expect(
            screen.queryByText("Wrong address format error")
          ).not.toBeInTheDocument();
        });
      });
    });

    describe("Given a wrong address in the input", () => {
      it("Shows a Wrong address format error' message", async () => {
        renderComponent();
        const input = screen.getByRole("textbox", { name: USER_ADDRESS_ID });
        userEvent.type(input, "wrong-address");
        expect(input).toHaveValue("wrong-address");
        // screen.getByText("Wrong address format error");
        const error = await screen.findByText("Wrong address format error");
        expect(error).toBeDefined();
        expect(error).toHaveTextContent("Wrong address format error");
        expect(screen.getByRole("button", { name: /submit/i })).toBeDisabled();
      });
    });
  });
  describe("[NO ERROR]", () => {
    describe("Given a a correct address in the input", () => {
      it('Dispatches a "change" event with the address on form "submit"', async () => {
        const spy = jest.fn();
        renderComponent(spy);
        const address = "0xrdgaerhatjha1561rgqare";
        const input = screen.getByRole("textbox", { name: USER_ADDRESS_ID });
        userEvent.type(input, address);
        expect(
          screen.queryByText("Wrong address format error")
        ).not.toBeInTheDocument();
        const submitBtn = screen.getByRole("button", { name: /submit/i });
        expect(submitBtn).toBeEnabled();
        userEvent.click(submitBtn);
        expect(spy).toHaveBeenCalledWith({ type: "change", address });
      });
    });
  });
});
