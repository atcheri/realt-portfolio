export type PortfolioState = {
  address: string;
};

const initialState: PortfolioState = {
  address: "",
};

export default initialState;
